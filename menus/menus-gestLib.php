<?php
// ==== menu: gestLib ==== //
$pagePath=PAGESLOCALES_ROOT.'/';
$mn='gestLib';
$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath.'accueil.php');
	// -- parametrer la page -- //
	$m->setAttr($p,'visible',1);				// 0: le li ne sera pas affiche 1:afficher
	$m->setAttr($p,'menuTitre','lib:gestLib');		// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
	$m->setAttr($p,'menuTitle','lib:gestLib');		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
	$m->setAttr($p,'titre','librairie gestLib');		// titre de la page: afficher dans le bas de page
//        $m->setMeta($p,'title','tutoriels - accueil(meta)');	// meta <title> (si non definit title=titre)
//	$m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>


$p='gestLib-js';
$m->addCallPage($p,$pagePath.$p.'.html');
        $m->setAttr("$p",'menuTitre','js');
        $m->setAttr("$p",'titre',"gestLib version js");
//        $m->addCssA("$p",'dossier1');
	
$p='gestLib-php';
$m->addCallPage($p,$pagePath.$p.'.php');
        $m->setAttr("$p",'menuTitre','php');
        $m->setAttr("$p",'titre',"gestLib version php");

?>
