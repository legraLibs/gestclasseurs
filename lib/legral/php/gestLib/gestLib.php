<?php
/*!******************************************************************
fichier: gestLib.php
auteur : Pascal TOLEDO
date de creation: 01 fevrier 2012
date de modification: 08 juin 2014
source: http://gitorious.org/gestLib
depend de:
	* aucune
description:
	* class de verification de librairie
	* fonction de debug avec niveau d'erreur
	* duree de parsing du fichier
documentation:
	* http://php.net/manual/fr/language.constants.predefined.php
*******************************************************************/
define ('GESTLIBVERSION','2.0.2');
//********************************************
// CLASS DE GESTION DES ERREURS
//********************************************

//if (gettype($gestLib)=='NULL')	// protection gestLib
//{
class LEGRALERR{
	const ALWAYS=-1;//sera toujours afficher quelque soit le niveau atrtribuer pour la lib
	const NOERROR=0;
	const CRITIQUE=1;
	const DEBUG=2;
	const WARNING=3;
	const INFO=4;
	const ALL=5;
}
function LEGRALERR_toString($err){
	switch ($err){
		case LEGRALERR::NOERROR :return 'NOERROR';
		case LEGRALERR::CRITIQUE :return 'CRITIQUE';
		case LEGRALERR::DEBUG :return 'DEBUG';
		case LEGRALERR::WARNING :return 'WARNING';
		case LEGRALERR::INFO :return 'INFO';
		case LEGRALERR::ALWAYS :return 'ALWAYS';
	}
}

//********************************************
// CLASS DE GESTION DES ETATS
//********************************************
class LEGRAL_LIBETAT{
	const NOLOADED=0;
	const LOADING=1;
	const LOADED=2;
}
function LEGRAL_LIBETAT_toString($etat){
	switch ($etat)
		{
		case LEGRAL_LIBETAT::NOLOADED :return 'NOLOADED';
		case LEGRAL_LIBETAT::LOADING :return 'LOADING';
		case LEGRAL_LIBETAT::LOADED :return 'LOADED';
		}
}

//********************************************
// function gestLib_format()
// -  formate les fonctions gestLib_inspect, debugShow(),debugShowVar()
//********************************************
function gestLib_format($txt,$f='br',$classe=''){
	switch($f){
		case '':case 'nobr':case NULL:return $txt;break;
		case 'ln':	$o="$txt\n";break;
		case 'div':	$o="<div class='$classe'>$txt</div>";break;
		case 'p':	$o="<p class='$classe'>$txt.</p>";break;
		case 'br':default: $o="$txt<br />";
		}
	return "$o";
}

//********************************************
// function gestLib_inspect() 
// - manipuler des variables independantes des libs - //
// - retourne les donnees d'une variable independante (nom,$var) au couleur de gestLib.css - //
// - si $varNom='' alors valeur seul - //
// - line,method: voir les info de debuggage - //
//********************************************
function gestLib_inspect($varNom,$var,$line=0,$methode='',$f='br',$classe=''){
        $out='';
	$t=gettype($var);
        if($varNom!='')$out.='<span class="gestLibVar">'.$varNom.'</span>='; // pas de nom preciser

        if ($line!==0){$out.="(<span class='gestLibInfo'>$line</span>)";}
        if ($methode!=''){$out.="[<span class='gestLibInfo'>$methode</span>]";}
	$t=gettype($var);

	$out.='<span class="gestLibVal">';
	$out.="[$t]";

	switch($t){
		case'boolean': case'integer': case'double':
		        if ($var===TRUE)  {$out.='TRUE strict ';}
        		if ($var===FALSE)  {$out.='FALSE strict ';}
			$out.=$var;
			break;
		case'string':
			$out.="'$var'";break;
		case'NULL':
			$out.='<i>NULL</i>';break;
		case'ressource':
			$out.='';break;
		case'array':
			$nb=count($var);
			if($nb>0){
				$out.="(array:$nb)<ul class='gestLibArray'>";
				foreach($var as $key => $value)$out.='<li style="list-style-type:decimal">'.gestLib_inspect($key,$value).'</li>';
				}
			$out.='</ul>';
                        break;

		case'object':
			$out.='(object:'.count($var).')<ol class="gestLibObject">';
			//if(get_class_vars($var))$out.='.';	// renvoie les valeurs par defaut
			if(!get_class_methods($var))$out.='<i>Classe sans methode.</i>';

			foreach($var as $key => $value)$out.='<li style="list-style-type:upper-roman">'.gestLib_inspect($key,$value).'</li>';
			$out.='</ol>';

			break;
		case'unknow type':
			$out.='<i>unknow type</i>';break;	
		default:
			$out.="(autre):$var";break;
	}

	if (empty($var)){$out.=' <i>empty</i> ';}
	$out.='</span>';
	return gestLib_format($out,$f,$classe);
}



//********************************************
// CLASS DE DONNEES
//********************************************
class gestLib_gestLib{
	public $nom=NULL;
	public $fichier=NULL;
	public $version=NULL;

	public $auteur='fenwe';
	public $site='http://legral.fr';
	public $git='https://git.framasoft.org/u/fenwe';
	public $description='';


	public $etat=NULL;	//1: en cours de chargement;2: chargement terminer

	function setEtat($etat){$this->etat=$etat;}
	var $dur=0;
        var $deb=0;
        var $fin=0;
	
	function getDuree(){return number_format($this->dur,6);}



function end()
	{
	$this->etat=LEGRAL_LIBETAT::LOADED;
	$this->fin=microtime(1);$this->dur=$this->fin - $this->deb;
	}


	public $err_level=0;
	public $err_level_Backup=NULL;	//sauvegarde de l'etat
	function setErr($err){$this->err_level=$err;}
	function setErrLevelTemp($err){$this->err_level_Backup=$this->err_level;$this->err_level=$err_level;}
	function setErrLevelTemp_NOERROR(){$this->err_level_Backup=$this->err_level;$this->err_level=LEGRALERR::NOERROR;}
	function restoreErrLevel(){$this->err_level=$this->err_level_Backup;}


function __construct($nom,$file,$version,$description=NULL)
	{
	$this->deb=microtime(1);
	$this->err_level=LEGRALERR::NOERROR;
	$this->nom=$nom;
	$this->fichier=$file;
	$this->version=$version;
	$this->etat=1;
	$this->description=$description;
	}
//function __toString() {return '';}

// - renvoie le texte si le niveau d'erreur concorde - //
function debugShow($level,$line='',$methode='',$txt='',$format='br',$classe='')
	{
	if($level>$this->err_level){return NULL;}
	$out='<span class="gestLibVar">'.$this->nom;
	$out.='{'.LEGRALERR_toString($level).'}';
	if ($line!==''){$out.="($line)";}
	if ($methode!==''){$out.="[$methode]";}
	$out.=':</span>';
	$out.='<span class="gestLibVal">'."$txt</span>";
	return gestLib_format($out,$format,$classe);
	}

// - renvoie le nom et la valeur (avec mise en forme) de la variable si le niveau d'erreur concorde - //
// function gestLib_inspect($varNom,$var,$line=0,$methode='')
	
function debugShowVar($level,$line,$methode,$varNom,$var,$format='br',$classe='')
	{global $gestlib;
	if ($level>$this->err_level){return '';}
	$out=gestLib_inspect($varNom,$var,$line,$methode,$format,$classe);
	return $out;
	}
}

//********************************************
// CLASS GESTIONLIBRAIRIE
//********************************************
class gestionLibrairies
{
public $libs=array();	//tableau de libs
//function __construct()	{	}

function __toString(){
	$out='<ul class="gestlib">';
	foreach($this->lib  as $key => $value)  $out.="<li>$key= $value</li>";$out.="</ul></li>";
	$out.='</ul>';
	return $out;
	}
function loadLib($nom,$file,$version,$description=NULL)
	{
//	if ( isset($this->libs[$nom]->nom) ){$this->libs[$nom]->nom=NULL;}//si deja charge
	$this->libs["$nom"]=new gestLib_gestLib($nom,$file,$version,$description);
	}
function tableau()
	{
	$out ='<table class="gestLib"><caption>Librairies PHP</caption>';
	$out.='<thead><tr><th>nom</th><th>version</th><th>etat</th><th>err level</th><th>durée</th><th>description</th><th>auteur</th> <th>git</th> </tr></thead>';
	foreach($this->libs as $key => $lib)//$lib= gestLib[index]
		{
//		$libNom=$lib->nom;
		$out.='<tr>';
		$out.='<td>'.$lib->nom.'</td>';
		$out.='<td>'.$lib->version.'</td>';
		$out.='<td>'.LEGRAL_LIBETAT_toString($lib->etat).'</td>';
		$out.='<td>'.LEGRALERR_toString($lib->err_level).'</td>';
		$out.='<td>'.$lib->getDuree().'</td>';
		$out.='<td>'.$lib->description.'</td>';
		$t=$lib->auteur;	$out.="<td><a target='exterieur' href='$lib->site'>$t</a></td>";
		$t=$lib->git;		$out.="<td><a target='git' href='$t'>$t</a></td>";
		$out.="</tr>\n";
		};

	$out.='</table>';
	return $out;
	}
function libTableau(){return $this->tableau();}

function setEtat($lib,$etat){$this->libs[$lib]->setEtat($etat);}
function getDuree($lib)     {$this->libs[$lib]->getDuree();}
function end($lib)          {$this->libs[$lib]->end();}
function getLibError($lib)  {if(isset($this->libs[$lib]))return LEGRALERR_toString($this->libs[$lib]->err_level);}

function debugShow($lib,$level,$line='',$methode='',$txt='',$format='br',$classe=''){
	if(isset($this->libs[$lib]))return $this->libs[$lib]->debugShow($level,$line,$methode,$txt,$format,$classe);
	}

// - accee aux libs - //
function debugShowVar($lib,$level,$line='',$methode='',$varNom='',$var='',$format='br',$classe=''){
	if(isset($this->libs[$lib]))return $this->libs[$lib]->debugShowVar($level,$line,$methode,$varNom,$var,$format,$classe);}

}	// class gestionLibrairie

// - creation d'une instance prefedinie - //
$gestLib= new gestionLibrairies();
$gestLib->loadLib('gestLib',__FILE__,GESTLIBVERSION,'gestionnaire de librairies');
$gestLib->libs['gestLib']->git='https://git.framasoft.org/legraLibs/gestlib';
$gestLib->end('gestLib');

//echo  gestLib_inspect('gestLib',$gestLib,__LINE__);
?>
