/*!
fichier: gestClasseurs-0.1.js
version:0.1
auteur:pascal TOLEDO
date: 2012.03.02
source: http://legral.fr/intersites/lib/perso/js/
depend de:
	* gestLib
	* prototype pour manipuler le dom (a refaire de facon independante)
description:
* gestion des de div superposé avec onglets de manipulations
* 
*/
if(typeof(gestLib)==='object')gestLib.loadLib({nom:'gestClasseurs',ver:0.1,description:'Gestionnaire de classeur par onglet'});

//options.classeuridJS
//option.feuilleNom
/*
function classeurs_menuVisible(options)
	{
	if(!options){return -1;}
	if(!options.classeuridJS){return -2;}
	if(!options.feuilleNom){return -3;}
	options.classeuridJS.show()
	}
*/

//options: nom:obligatoire
//erreur:
// -1: absence d'options
// -10: la requete Ajax a echouer


function TclasseurFeuille(classeurNom,options)
	{
	this.erreur=0;
	this.methodRun='TclasseurFeuille';
	this.isLoad=0;
	if(!classeurNom)	 {this.erreur=-2;return -2;}	this.classeurNom=classeurNom;
	if(!options)	 	 {this.erreur=-1;return -1;}	this.options=options;
	if(!this.options.nom){this.erreur=-3;return -3;}	this.nom=options.nom;	//nom de la feuille et id creer dyn
//	if(!this.options.classeuridJS){this.erreur=-4;return -4;}	this.nom=options.classeuridJS;	//var pointant 
	this.titre=(this.options.titre)?this.options.titre:this.options.nom;
	this.support=this.classeurNom+'_'+this.nom;
//	this.classeuridJS=options.classeuridJS;
	this.datas_supportidCSS=document.getElementById(this.classeurNom+'_datas_support');	//conteneur des zones datas
	this.liidCSS=null;
	this.datasidCSS=null;
	this.description=options.description;
	this.texte='';
//	this.ajaxStatus=0;//reponse ajax

	if (this.erreur){return this.erreur;}
	this.loadFeuille();
	this.isLoad=1;
	return this;
	} // class Tclasseur

TclasseurFeuille.prototype=
	{
	destruct:function()
		{
		document.getElementById(this.support+'_li').remove();
		document.getElementById(this.support+'_datas').remove();
		this.feuille=undefined;
		}
	,loadFeuille:function()
		{
		this.methodRun='TclasseurFeuille:loadFeuille';

		//insertion de l'elt:onglet
		document.getElementById(this.classeurNom+'_onglets_ul').insert({bottom:new Element('li',{id:this.support+'_li'})});
		this.liidCSS=document.getElementById(this.support+'_li');
		this.liidCSS.insert({top:new Element('span',{id:this.support+'_span',
			onclick:this.classeurNom+'.show("'+this.nom+'")'})});
		document.getElementById(this.support+'_span').innerHTML=this.titre;
//		Event.observe(this.support+'_li','click',function(){classeur1_menuVisible(this.nom);});

		//insertion de l'elt:contenue
		this.datas_supportidCSS.insert({bottom:new Element('div',{id:this.support+'_datas','class':this.classeurNom+'_datas'})});
		this.datasidCSS=document.getElementById(this.support+'_datas');
		this.datasidCSS.innerHTML=this.nom;
		//contenue
//		if(options.url){}
		}
	,show:function(all){this.liidCSS.style.visibility='visible';this.datasidCSS.style.visibility='visible';}
	,hide:function(all)
		{
		if(all){this.liidCSS.style.visibility='hidden';}
		this.datasidCSS.style.visibility='hidden';
		}
	,write:function(txt){this.texte+=txt;this.datasidCSS.innerHTML=this.texte;}
	}


function Tclasseur(options)
	{
	this.erreur=0;
	this.methodRun='Tclasseur';
	if(!options){this.erreur=-1;return -1;}
	if(!options.nom){this.erreur=-2;return -2;}
	this.options=options;
	this.nom=options.nom;
	this.htmlID=(options.htmlID)?options.htmlID:options.nom;
	this.htmlIDCSS=document.getElementById(this.htmlID);
	if(!this.htmlIDCSS){this.erreur=-3;return -3;}
	this.classeur_datas_supportidCSS=null;
	this.feuilleNb=0;
	this.feuilleDetruiteNb=0;
	this.feuilles=new Array();
	this.creerSupport();
	return this;
	}//class Tclasseur

Tclasseur.prototype =
	{
	destruct:function(feuille)
		{
		if(feuille)
			{if(this.feuilles[feuille]!=undefined&&'unload')
				{this.feuilles[feuille].destruct();this.feuilles[feuille]='unload';this.feuilleDetruiteNb++;}
				}
		else	{
			var feuilleNu=0;
			for(feuiller in this.feuilles)
				{
				this.feuilles[feuiller].destruct();
				this.feuilles[feuiller]='unload';
				feuilleNu++;
				if(feuilleNu>=this.feuilleNb){break;}
				}
			this.feuilles='unload';
			this.feuilles=new Array();
			this.feuilleDetruiteNb=0;
			this.feuilleNb=0;
			}
		},
	clean:function(nom)
		{var temp=new Array();
		for (var key in this.feuilles)
			{
			if(this.feuilles[key]!='unload'&&key!=nom){temp[key]=this.feuilles[key];}
			this.feuilleNb--;
			}
		this.feuilles=temp;
		},
		
	creerSupport:function()
		{
		this.htmlIDCSS.insert({bottom:new Element('div',{id:this.nom+'_onglets'})});//<div id="classeur1_onglets">
		this.classeur_ongletsidSS=document.getElementById(this.nom+'_onglets');

			this.classeur_ongletsidSS.insert({bottom:new Element('ul',{id:this.nom+'_onglets_ul'})});//<div id="classeur1_onglets_ul">
			this.classeur_onglets_ulidSS=document.getElementById(this.nom+'_onglets_ul');

		this.htmlIDCSS.insert({bottom:new Element('div',{'class':'clear'})});//<div class="clear"></div>
		this.htmlIDCSS.insert({bottom:new Element('div',{id:this.nom+'_datas_support'})});//
		this.classeur_datas_supportidCSS=document.getElementById(this.nom+'_datas_support');//<div id="classeur1_datas_support"></div>
		},

	loadFeuille:function(options)
		{
		this.methodRun='Tclasseur';
		this.erreur=0;
		if(!options){this.erreur=-1;return -1;}
		if(!options.nom){this.erreur=-2;return -2;}
		
		if(this.feuilles[options.nom]==undefined||options.forcer)
			{
			this.destruct(options.nom);
			this.feuilles[options.nom]=new TclasseurFeuille(this.nom,options);
//			Event.observe(this.nom+'_'+options.nom+'_li','click',
//				function()
//					{
//					this.show(options.nom);
//					});
			this.feuilleNb++;
//			gestLib.inspect({lib:'gestClasseur',varNom:'this.feuilles['+options.nom+'].isLoad',varPtr:'(loadFichier)'+this.feuilles[options.nom].isLoad});
//			gestLib.inspect({lib:'gestClasseur',varNom:'this.feuilles['+options.nom+'].erreur',varPtr:'(loadFichier)'+this.feuilles[options.nom].erreur});

//			if(!this.feuilles[options.nom].ajax.success())
			//if (this.feuilles[options.nom].isLoad==0)
			if(this.feuilles[options.nom].erreur<0)
				{
				this.feuilleNb--;
				erreur=this.feuilles[options.nom].erreur;
				this.destruct(this.options.nom);
				return erreur;}
			}
		return 0;
		},
	show:function(nom)
		{

		if(this.feuilles[nom]!=undefined&&this.feuilles[nom]!='unload')
			{
			this.hide();
			this.feuilles[nom].show();
			}
		},
	hide:function(nom)
		{
		if(!nom)
			{
			for(var key in this.feuilles)
				{

				//if(this.feuilles[key]!=undefined&&this.feuilles[key]!='unload')
				if(this.feuilles[key] && this.feuilles[key].nom)
					{this.feuilles[key].hide();}
				}
			}
		else	{

			if(this.feuilles[nom] && this.feuilles[nom].nom)
				{this.feuilles[nom].hide();}
			}
		},
	write:function(options)
		{
		this.methodRun='Tclasseur:write';this.erreur=0;
		if(!options){this.erreur=-1;return -1;}

		if(this.feuilles[options.feuille]!=undefined&&this.feuilles[options.feuille]!='unload')
			{this.feuilles[options.feuille].write(options.txt);}
		}
	}

/************************************************************/
if(typeof(gestLib)==='object')gestLib.end('gestClasseurs');
