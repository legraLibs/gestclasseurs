<h1 class="h1">gestClasseurs - howto</h1>

<h2 class="h2">inclusions</h2>

<!--span class="coding_filename">&lt;script src="./gestClasseurs.js"&gt;&lt;script&gt;</span><br-->
<pre class="coding_code ">&lt;script src="./gestClasseurs.js"&gt;&lt;script&gt;</pre>
<pre class="coding_code ">&lt;link rel="stylesheet" href="gestClasseurs.css" media="all" /&gt;</pre>

<h2 class="h2">html: création de la div support</h2>

Une div support doit etre creer avec une id.
<pre class="coding_code ">&lt;div id="classeurTest" class="gestClasseursSupport"&gt;&nbsp;support du classeur&lt;/div&gt;</pre>
<div id="classeurTest" class="gestClasseursSupport">&nbsp;support du classeur</div>;


<h2 class="h2">Manipulation par javascript</h2>

<h3 class="h3">création de l'instance d'un classeur</h3>
<pre class="coding_code ">classeurTest=new Tclasseur({"nom":"classeurTest"});</pre>
<script>classeurTest=new Tclasseur({"nom":"classeurTest"});</script>


<h3 class="h3">charger une feuille</h3>

<ul>
	<li>nom: nom interne: pas d'espace ni de caracteres spéciaux</li>
	<li>titre: ce qui sera affiché dasn l'onglet de la feuille</li>
	<li>texte: texte afficher à la cration de la feuille</li>
	<li></li>
</ul>
<pre class="coding_code">classeurTest.loadFeuille({"nom":"accueil","titre":"accueil général","texte":"accueil"})</pre>
<script>classeurTest.loadFeuille({"nom":"accueil","titre":"accueil général","texte":"accueil"})</script>

<pre class="coding_code">classeurTest.loadFeuille({"nom":"feuilleCache","titre":"debug",<b>"hide":1</b>,"texte":"Ce texte ne pourra être lu."})</pre>
<script>classeurTest.loadFeuille({"nom":"feuilleCache","titre":"feuille caché","hide":1,"texte":"Cette feuille sera caché à sa création"})</script><pre class="coding_code">classeurTest.loadFeuille({"nom":"feuilleSelectionnee","titre":"feuille séléctionnée","texte":"Cette feuille sera séléctionnée par le javascript"})</pre>

<pre class="coding_code">classeurTest.loadFeuille({"nom":"feuilleSelectionnee","titre":"feuille séléctionnée","texte":"Cette feuille sera séléctionnée par le javascript"})</pre>
<script>classeurTest.loadFeuille({"nom":"feuilleSelectionnee","titre":"feuille séléctionnée","texte":"Cette feuille sera séléctionnée par le javascript"})</script>

<pre class="coding_code">classeurTest.loadFeuille({"nom":"debug","titre":"debug","texte":"debug"})</pre>
<script>classeurTest.loadFeuille({"nom":"debug","titre":"sortie de debug","texte":"debug"})</script>



<h3 class="h3">Ecrire dans une feuille</h3>

<pre class="coding_code ">classeurTest.write({feuille:'accueil',texte:'ce texte remplacera le contenu de la feuille'});</pre>
<script>classeurTest.write({feuille:'accueil',texte:'ce texte remplacera le contenu de la feuille'});</script>

<pre class="coding_code">classeurTest.append({feuille:'accueil',texte:'<br>ce texte sera ajouté au contenu de la feuille'});</pre>
<script>classeurTest.append({feuille:'accueil',texte:'<br>ce texte sera ajouté au contenu de la feuille'});</script>


<h3 class="h3">Rendre invisible une feuille (<span class="pointeur" onclick="classeurTest.hide('feuilleCache');">éxécuter le code</span>)</h3>
<pre class="coding_code">classeurTest.hide('feuilleCache');</pre>
<!--script>classeurTest.hide('feuilleCache');</script-->

<h3 class="h3">Rendre visible une feuille (<span class="pointeur" onclick="classeurTest.show('feuilleCache');">éxécuter le code</span>)</h3>
<pre class="coding_code">classeurTest.show('feuilleCache');</pre>
<!--script>classeurTest.show('feuilleCache');</script-->

<h3 class="h3">Selectionnée une feuille</h3>
<script>classeurTest.select('feuilleSelectionnee');</script>





