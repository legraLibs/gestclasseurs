/*!
fichier: gestClasseurs.js
auteur:pascal TOLEDO
date de creation : 2014.12.05
date de modification: 2015.04.20
depend de:
  * rien
description: cf README
*/

GESTCLASSEURSVERSION= '0.2.1';

if(typeof(gestLib)==='object')gestLib.loadLib({nom:'gestClasseurs',ver:GESTCLASSEURSVERSION,description:'creation de div dynamique en js',isConsole:0,isVisible:0,url:'https://git.framasoft.org/legraLibs/gestclasseurs'});


//options: nom:obligatoire
//erreur:
// -1: absence d'options
// -10: la requete Ajax a echouer


/*
 * function TclasseursFeuille(classeurNom,options)
 * param obligatorie:
 * nom: pas d'espace ni de caractere spéciaux (sert a calcule l'IDHTML des enfants)
 * classeurIDJS
 */
function TclasseursFeuille(f){
	this.methodRun='TclasseurFeuille';
	this.isLoad=0;
	if(typeof(f)!='object')return -1;
	this.nom=f.nom;				// pas d'espace ni de caracteres spéciaux (sert a calcule l'IDHTML des enfants)

	// - - //
	this.classeurIDJS=f.classeurIDJS;
	this.description=f.description;	// description de la feuille

	// -  menu: support des onglets - //
	this.menuIDCSS=this.classeurIDJS.menuIDCSS;
	this.ongletIDCSS=null;
	this.titre=(f.titre)?f.titre:f.nom;	// titre de l'onglet (texte afficher)

	// - datasBoxSupport - //
	this.datasBoxSupportIDCSS=this.classeurIDJS.datasBoxSupportIDCSS;
	this.datasBoxIDCSS=null;	// - IDCSS de la dataBox contenant le texte
	this.texte=f.texte;			// -  contenu de la dataBox
	//	this.liidCSS=null;

	this.description=f.description;

	// - creation de la feuille: (onglet + dataBox) - //
	this.loadFeuille();
	this.isLoad=1;

	if(f.hide===1)this.hide();	// cacher la feuille si demandé
	return this;
} // class Tclasseur


TclasseursFeuille.prototype={
	destruct:function()
		{
		document.getElementById(this.support+'_li').remove();
		document.getElementById(this.support+'_datas').remove();
		this.feuille=undefined;
		}
	,onClick:function(elt,f){
	}

	,loadFeuille:function(){
		this.methodRun='TclasseurFeuille:loadFeuille';

		// - creation de l'onglet dans le menu - //
		this.ongletIDCSS=document.createElement('span');
		this.ongletIDCSS.id=this.classeurIDJS.nom+'_'+this.nom+'_onglet';	// utile?
		this.ongletIDCSS.className="gestClasseurs_onglet";
		this.ongletIDCSS.innerHTML=this.titre;
		this.menuIDCSS.appendChild(this.ongletIDCSS);

		var _classeurIDJS=this.classeurIDJS; // pour envoyer dans la fonction anonyme
		var _feuilleIDJS=this;

		// - activation du click sur le menu - //
		this.ongletIDCSS.onclick=function(elt){
//				gestLib.inspect({"lib":"gestClasseurs","varNom":"_classeurIDJS.nom","varPtr": _classeurIDJS.nom});
//				gestLib.inspect({"lib":"gestClasseurs","varNom":"_feuilleIDJS.nom","varPtr":  _feuilleIDJS.nom});
				_classeurIDJS.select(_feuilleIDJS.nom);
				
				}



		// - creation de la dataBox dans la datasBoxSupport - //
		this.datasBoxIDCSS=document.createElement('div');
		this.datasBoxIDCSS.id=this.classeurIDJS.nom+'_'+this.nom+'_datasBox';	// utile?
		this.datasBoxIDCSS.className="gestClasseurs_datasBox";
		this.datasBoxIDCSS.innerHTML=this.texte;
		this.datasBoxSupportIDCSS.appendChild(this.datasBoxIDCSS);


	} // loadFeuille

	// - selectionne l'onglet: le met au 1er plan et applique le style - //
	,select:function(){
		// -- mise en style -- //
		this.ongletIDCSS.className+=' gestClasseurs_ongletSelected';
		this.datasBoxIDCSS.style.display='block';

	}

	,unselect:function(){
		// -- supprime le style -- //
		this.ongletIDCSS.className=this.ongletIDCSS.className.replace(' gestClasseurs_ongletSelected','');
		this.datasBoxIDCSS.style.display='none';
	}

	// - affiche l'onglet et la dataBox - //
	,show:function(){
		this.ongletIDCSS.style.display='inline';
		this.datasBoxIDCSS.style.display='block';
	}

	// - rend invisible l'onglet et la box - //
	,hide:function(){
		this.ongletIDCSS.style.display='none';
		this.datasBoxIDCSS.style.display='none';
		}

	,write:function(texte){this.datasBoxIDCSS.innerHTML=texte;}

	,append:function(texte){this.datasBoxIDCSS.innerHTML+=texte;}
}

/*********************
 *
 *
 ********************/

function Tclasseur(c)
	{
	this.erreur=0;
	this.methodRun='Tclasseur';
	if(typeof(c)!='object'){this.erreur=-1;return -1;}
	if(!c.nom){this.erreur=-2;return -2;}
	this.nom=c.nom;

	// - support general - //
	this.htmlID=(c.htmlID)?c.htmlID:c.nom;
	this.htmlIDCSS=document.getElementById(this.htmlID);
	if(!this.htmlIDCSS){this.erreur=-3;return -3;}


	// - support du menu des onglets - //
	this.menuIDCSS=null;
	this.datasBoxSupportIDCSS=null;
	this.menuTitre=c.menuTitre?c.menuTitre:'';

	// - - //
	this.feuilleNb=0;
	this.feuilleDetruiteNb=0;
	this.feuilles=new Array();
	this.creerSupport();
	return this;
	}//class Tclasseur

Tclasseur.prototype ={
	destruct:function(feuille){
		if(feuille)
			{if(this.feuilles[feuille]!=undefined&&'unload')
				{this.feuilles[feuille].destruct();this.feuilles[feuille]='unload';this.feuilleDetruiteNb++;}
				}
		else	{
			var feuilleNu=0;
			for(feuiller in this.feuilles)
				{
				this.feuilles[feuiller].destruct();
				this.feuilles[feuiller]='unload';
				feuilleNu++;
				if(feuilleNu>=this.feuilleNb){break;}
				}
			this.feuilles='unload';
			this.feuilles=new Array();
			this.feuilleDetruiteNb=0;
			this.feuilleNb=0;
			}
	},

	clean:function(nom){var temp=new Array();
		for (var key in this.feuilles)
			{
			if(this.feuilles[key]!='unload'&&key!=nom){temp[key]=this.feuilles[key];}
			this.feuilleNb--;
			}
		this.feuilles=temp;
	},
		
	creerSupport:function(){

		// - creation du support des menus - //
		this.menuIDCSS=document.createElement('div');
		this.menuIDCSS.id=this.nom+'_onglets';	// utile?
		this.menuIDCSS.className="gestClasseurs_menuSupport";
		this.menuIDCSS.innerHTML=this.menuTitre;
		this.htmlIDCSS.appendChild(this.menuIDCSS); 

		// - creation du support des dataBox - //
		this.datasBoxSupportIDCSS=document.createElement('div');
		this.datasBoxSupportIDCSS.id=this.nom+'_datasBoxSupport';	// utile?
		this.datasBoxSupportIDCSS.className="gestClasseurs_datasBoxSupport";
		this.datasBoxSupportIDCSS.innerHTML="datasBoxSupport";
		this.htmlIDCSS.appendChild(this.datasBoxSupportIDCSS); 
	}

	,loadFeuille:function(f){
		this.methodRun='Tclasseur';
		this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}
		if(!f.nom){this.erreur=-2;return -2;}		// la feuille n'a pas de nom
		
		if(this.feuilles[f.nom]==undefined||f.forcer){
			this.destruct(f.nom);

			f.classeurIDJS=this;

			this.feuilles[f.nom]=new TclasseursFeuille(f);

			// - supprimer la feuille en cas d'erreur - //
			if(this.feuilles[f.nom].erreur<0){
				this.feuilleNb--;
				erreur=this.feuilles[f.nom].erreur;
				this.destruct(this.f.nom);
				return erreur;
			}
		}

	}// loadFeuille()

	// - met la feuille au 1er plan (ne l'affiche pas si elle est caché)  - //
	,select:function(nom){
		var f=this.feuilles[nom];
		if(f != undefined) {
			this.unselectAll();
			f.select();
		}
	}
	
	,unselectAll:function(){
		for(var key in this.feuilles){
			var f=this.feuilles[key];
			if(f && f.nom)
				{f.unselect();}
		}

	}

	// - rend visible l'onglet et la dataBox - //
	,show:function(nom){
		// - si aucun nom de feuille n'est donnee alors les afficher toutes - //
		if(!nom){
			for(var key in this.feuilles)
				{
				if(this.feuilles[key] && this.feuilles[key].nom)
					{this.feuilles[key].show();}
				}
		}
		else{
			if(this.feuilles[nom] && this.feuilles[nom].nom)
				{this.feuilles[nom].show();}
		}
	}

	,hide:function(nom){
		// - si aucun nom de feuille n'est donnee alors les cacher toutes - //
		if(!nom){
			for(var key in this.feuilles)
				{
				if(this.feuilles[key] && this.feuilles[key].nom)
					{this.feuilles[key].hide();}
				}
		}
		else{
			if(this.feuilles[nom] && this.feuilles[nom].nom)
				{this.feuilles[nom].hide();}
		}
	},

	write:function(f){
		this.methodRun='Tclasseur:write';this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}

		var fe=this.feuilles[f.feuille];
		if(fe != undefined && fe != 'unload')
			{fe.write(f.texte);}
	}

	,append:function(f){
		this.methodRun='Tclasseur:append';this.erreur=0;
		if(typeof(f)!='object'){this.erreur=-1;return -1;}

		var fe=this.feuilles[f.feuille];
		if(fe != undefined && fe != 'unload')
			{fe.append(f.texte);}
	}

} // Tclasseur.prototype



// - enregistrement de la librairie dans le gestionanire de librairie - //
if(typeof(gestLib)==='object')gestLib.end('gestClasseurs');
