#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier


#######################################
# updateModele()                        #
# met les lib a jours (par linkage) #
#######################################
updateModele(){
	echo "$couleurINFO # - localVersion.sh:updateModele() - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		mkdir -p ./scripts/;
		mkdir -p ./styles

		# -- gestionnaire de versions -- #
		echo "";
		lib='gitVersion';
		#version dev
		libV="$lib.sh";echo "$couleurINFO lib:$libV $couleurNORMAL";link /www/git/bash/$lib/scripts/$libV ./scripts/$libV;
		#version fixe
		libV="$lib-v2.0.1.sh";echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/bash/$lib/versions/scripts/$libV ./scripts/$libV;

		# -- librairies  -- #
		echo "";
		lib='gestLib';
		mkdir -p ./lib/legral/php/$lib;
		#version dev
		libV="$lib.php";echo "$couleurINFO linkage de $libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV		./lib/legral/php/$lib/$libV;
		libV="$lib.js"; echo "$couleurINFO linkage de $libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV		./lib/legral/php/$lib/$libV;
		libV="$lib.css";echo "$couleurINFO linkage de $libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/styles/$libV	./lib/legral/php/$lib/$libV;
		#version fixe
		libV="$lib-v2.0.0.php";echo "$couleurINFO copie de $libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib-v2.0.0.js" ;echo "$couleurINFO copie de $lib $couleurNORMAL"; cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		#libV="$lib-v2.0.0.css";echo "$couleurINFO copie de $lib $couleurNORMAL"; cp /www/git/intersites/lib/legral/php/$lib/styles/$libV   ./lib/legral/php/$lib/$libV;

		lib='menuStylisee';
		mkdir -p ./lib/legral/php/$lib;
		libV="$lib.php";echo "$couleurINFO linkage de $libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib.css";echo "$couleurINFO linkage de $libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#libV="$lib-v0.1.php";echo "$couleurINFO copie de $libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;

		lib='gestMenus';
		mkdir -p ./lib/legral/php/$lib;
		libV="$lib.php";echo "$couleurINFO linkage de $libV $couleurNORMAL";  link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib.css";echo "$couleurINFO linkage de $libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#libV="$lib-v1.14.0.php";echo "$couleurINFO copie de $libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		#libV="$lib-v1.14.0.css";echo "$couleurINFO copie de $libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;

		# -attention lib non formalise -#
		#lib='gestLogin';
		#mkdir -p ./lib/legral/php/$lib;
		#libV="$lib.php";echo "$couleurINFO linkage de $libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#libV="$lib-v.php";echo "$couleurINFO copie de $couleurNORMAL"; cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;

		#lib='gestPDO';
		#mkdir -p ./lib/legral/php/$lib;
		#version dev (cette lib n'est pas formalisee)
#		libV="$lib.php";echo "$couleurINFO lib:$libV $couleurNORMAL";  link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#version fixe
		#libV="$lib-v1.1.0.php";echo "$couleurINFO copie de $libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;


		# -- styles communs -- #
		css='';
		cssV=$css'knacss.css';echo "$couleurINFO copie de $cssV $couleurNORMAL";cp /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='';
		cssV=$css'html4.css';echo "$couleurINFO linkage de $cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='';
		cssV=$css'intersites.css';echo "$couleurINFO linkage de $cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='notes';
		cssV=$css'Relatives.css';echo "$couleurINFO linkage de $cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='tutoriels';
		cssV=$css'Relatif.css';echo "$couleurINFO linkage de $cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		# -- menus communes -- #
		echo "";
		echo "$couleurINFO pages communes $couleurNORMAL";
		mkdir -p ./menus/
		f='menus/menus-systeme.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/$f $f;

		# -- pages communes -- #
		echo "";
		echo "$couleurINFO pages communes $couleurNORMAL";
		mkdir -p ./pagesLocales/_site/
		f='_site/about.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;
		f='_site/credits.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;
		f='_site/inspect.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;
		f='_site/plans.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;


		# -- fichiers communs -- #
		echo "";
		echo "$couleurINFO fichiers communs $couleurNORMAL";
		f='index.php';echo "$couleurINFO copie de $f $couleurNORMAL";cp --no-clobber /www/git/gitModele/$f ./$f;
		f='piwik.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/$f ./$f;

		# -- Changement des droits  -- #
		echo "";
		echo "$couleurINFO Changement des droits $couleurNORMAL";
		f='./scripts/';	echo "$couleurINFO chmod -R 755 $f $couleurNORMAL";chmod -R 755 $f
		f='./styles/';	echo "$couleurINFO chmod -R 755 $f $couleurNORMAL";chmod -R 755 $f
		f='./menus/';	echo "$couleurINFO chmod -R 755 $f $couleurNORMAL";chmod -R 755 $f
		f='./lib/';		echo "$couleurINFO chmod -R 755 $f $couleurNORMAL";chmod -R 755 $f
		f='./pagesLocales/';	echo "$couleurINFO chmod -R 755  $f $couleurNORMAL";chmod -R 755 $f
	fi
}


#########################################################
# localConcat()                                         #
# concatenne les fichiers css dans ./styles/styles.css  #
# concatenne les fichiers js  dans ./locales/scripts.js #
#########################################################
localConcat() {
        # - concatenation des fichiers css - #
        echo "$couleurINFO # - concatenation des fichiers css dans ./styles/styles.css - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then

		f='./styles/knacss.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f

		f='./styles/html4.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f
		f='./styles/intersites.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f

		f='./lib/legral/php/gestLib/gestLib.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f
		#f='./lib/legral/php/gestLib/versions/gestLib-v2.0.0.css';echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f
	
		f='./lib/legral/php/menuStylisee/menuStylisee.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f
		f='./lib/legral/php/gestMenus/gestMenus.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f

		f='./styles/notesRelatives.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f
		f='./styles/tutorielsRelatif.css';	echo "$couleurINFO catCSS de $f $couleurNORMAL";catCSS $f

	fi

        # - concatenation des fichiers js - #
	echo "";
        echo "$couleurINFO # - concatenation des fichiers js dans ./locales/scripts.js  - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		f='/www/git/intersites/lib/legral/php/gestLib/gestLib.js';	echo "$couleurINFO catJS de $f $couleurNORMAL";catJS $f
	fi
	}


##################################################
# localSave()                                    #
# copie un fichier en le suffixant de la version #
##################################################
localSave() {
	echo "$couleurINFO versionning des fichiers locaux$couleurNORMAL";
	echo "$couleurWARN Rien à versionner $couleurNORMAL";return 1;	# a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		echo "$couleurWARN Rien à versionner $couleurNORMAL";return 1;	# a commenter une fois parametré

		# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
		#versionSave ./scripts/gitVersion sh
		#versionSave ./localVersion sh
		versionSave gestClasseurs js
		versionSave gestClasseurs css
	fi
	}


#######################################
# localStatification()                #
# télécharge et rend statique un site #
#######################################
localStatification(){
	echo "$couleurINFO # - localVersion.sh:localStatification() - #$couleurNORMAL";
	lib='gestClasseurs';
	url="http://127.0.0.1/git/intersites/lib/legral/js/$lib";

	echo "telechargement avec rendu statique d'un site de $url";
	echo "$couleurINFO la version statique se trouve: $url/statique $couleurNORMAL";

	if [ $isSimulation -eq 0 ];then

		echo "$couleurINFO suppression et creation de ../statique/$couleurNORMAL";
		rm -R ./statique/;mkdir ./statique/;cd   ./statique/;

		echo "$couleurINFO téléchargement...$couleurWARN";

		#--no-verbose --quiet
		wget --quiet --tries=5 --continue --no-host-directories --html-extension --recursive --level=inf --convert-links --page-requisites --no-parent --restrict-file-names=windows --random-wait --no-check-certificate $url

		echo "$couleurINFO deplacer et nettoyer le chemin$couleurNORMAL";
		mv ./git/intersites/lib/legral/js/$lib/ ./

		# - decommenter pour activer la suppression apres verification - #
		rm -R ./git/

	fi
}


#######################################
# syncRemote()                        #
# synchronise le(s) serveurs distants #
#######################################
syncRemote(){
	lib='gestClasseurs';
	urlDest="intersites/lib/legral/php/$lib";
	echo "$couleurINFO url: http://legral.fr/$urlDest $couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		#echo "$couleurWARN # - pas de Remote - #$couleurNORMAL";return 1; # a commenter une fois parametré
		#echo "exemple:";
		lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./statique/$lib   /www/$urlDest ; quit"
	fi
}


#################
# postGit()     #
# lancer en fin #
#################
postGit(){
	echo "$couleurINFO # - localVersion.sh:postGit() - #$couleurNORMAL";
	echo "$couleurWARN Pas de postGit $couleurNORMAL";return 1; # a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		echo "$couleurWARN Pas de postGit $couleurNORMAL";return 1; # a commenter une fois parametré
	fi
}
